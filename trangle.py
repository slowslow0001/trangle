#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import numpy as np
import math
import sympy as sy
import random
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse, Circle

#求得最近beacon
def scan_beacon(real_location,beacon_location):
	beacon_distance = [] # 實際座標到各個beacon的距離
	for i in beacon_location:
		distanceXY = ((real_location[0] - i[0]) ** 2  + (real_location[1] - i[1]) ** 2)
		distance = int(round(math.sqrt(distanceXY)))
		beacon_distance.append(distance)
	print "real:" + str(real_location) # 實際座標
	print "random beacon:" + str(beacon_location) # beacon集合的位置
	print "total distance:"+str(beacon_distance) # 實際座標到各個beacon的距離
	beacon_id = [] # 最近的三顆 beacon 的 索引
	rssi_distance = []
	for j in range(3): 
		i = 0
		while i < len(beacon_distance):
			if min(beacon_distance) == beacon_distance[i]:
				beacon_id.append(i)
				rssi_distance.append(min(beacon_distance))
				beacon_distance[i] = "get"
				break
			else:
				pass
				i = i + 1
	# print beacon_id
	# print rssi_distance
	return (beacon_location[beacon_id[0]],beacon_location[beacon_id[1]],beacon_location[beacon_id[2]],rssi_distance[0],rssi_distance[1],rssi_distance[2])


def location(d1,d2,d3,b1,b2,b3):
	x1 = sy.Symbol("x1")
	y1 = sy.Symbol("y1")
	x2 = sy.Symbol("x2")
	y2 = sy.Symbol("y2")
	x3 = sy.Symbol("x3")
	y3 = sy.Symbol("y3")
	f1 = (x1 - b2[0]) ** 2 + (y1 - b2[1]) ** 2 - (d2 ** 2)
	f2 = (x1 - b3[0]) ** 2 + (y1 - b3[1]) ** 2 - (d3 ** 2)
	p1 = sy.solve((f1,f2), x1, y1)
	f3 = (x2 - b1[0]) ** 2 + (y2 - b1[1]) ** 2 - (d1 ** 2)
	f4 = (x2 - b3[0]) ** 2 + (y2 - b3[1]) ** 2 - (d3 ** 2)
	p2 = sy.solve((f3,f4), x2, y2)
	f5 = (x3 - b1[0]) ** 2 + (y3 - b1[1]) ** 2 - (d1 ** 2)
	f6 = (x3 - b2[0]) ** 2 + (y3 - b2[1]) ** 2 - (d2 ** 2)
	p3 = sy.solve((f5,f6), x3, y3)
	# print "p1:" +str(p1)
	# print "p2:" +str(p2)
	# print "p3:" +str(p3)
	localx = abs((p1[0][0] + p2[0][0] + p3[0][0]) / 3)
	localy = abs((p1[0][1] + p2[0][1] + p3[0][1]) / 3)
	# print "(" +str(localx) + "," + str(localy) + ")"
	return localx,localy

# 畫出訊號範圍圖
def plot_triangle(A, B, C,real_location,predict_location,beacon_location):
	x = [A[0], B[0], C[0]]
	y = [A[1], B[1], C[1]]

	ax = plt.gca()
	ax.plot(x, y,"ro",label = "near")

	ax.plot(predict_location[0],predict_location[1],"ro",color="yellow",label="predict")
	ax.plot(real_location[0],real_location[1],"ro",color="green",label = "real")


	for i in beacon_location:
		ax.plot(i[0],i[1],"ro",color="blue")


	plt.legend(loc='upper right')

	plt.xticks(np.linspace(0, 50, 10))
	plt.yticks(np.linspace(0, 50, 10))
	plt.show()

def main():
	try:
		beacon_location = []
		for i in range(10):
			beacon = (random.randint(1,40),random.randint(1,40))
			beacon_location.append(beacon)
		real_location = random.randint(1,40),random.randint(1,40)
		near_beacon = scan_beacon(real_location,beacon_location) 
		print "result:"+str(near_beacon) # 顯示離實際座標最接近的三顆beacon 座標、距離
		predict_location = location(near_beacon[3],near_beacon[4],near_beacon[5],near_beacon[0],near_beacon[1],near_beacon[2])
		
		beacon_location.remove(near_beacon[0])
		beacon_location.remove(near_beacon[1])
		beacon_location.remove(near_beacon[2])
		plot_triangle(near_beacon[0],near_beacon[1],near_beacon[2],real_location,predict_location,beacon_location)
		# print beacon_location
	except KeyboardInterrupt:
		pass
	finally:
		pass
		# os.system("clear")

if __name__=="__main__":	
	main()